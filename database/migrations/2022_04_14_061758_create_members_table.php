<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('fname',100);
            $table->string('lname',100);
            $table->string('birth_day');
            $table->string('gender');
            $table->string('email',100);
            $table->string('phone',20);
            $table->string('standard',100);
            $table->string('joining_date');
            $table->string('photo',100);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
};

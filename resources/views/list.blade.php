<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
     <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <title>students lsit</title>
  </head>
  <body>
  <section class="vh-100 gradient-custom-2">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-md-12 col-xl-10">

        <div class="card mask-custom">
          <div class="card-body p-4 text-white">

            <div class="text-center pt-3 pb-2">
              <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-todo-list/check1.webp" alt="Check" width="60">
              <h2 class="my-4">Student List</h2>
            </div>

            <table class="table text-white mb-0">
              <thead>
                <tr>
                  <th scope="col">Name</th>
                  <th scope="col">DOB</th>
                  <th scope="col">gender</th>
                  <th scope="col">email</th>
                  <th scope="col">phone</th>
                  <th scope="col">standard</th>
                  <th scope="col">joining date</th>
                  
                </tr>
              </thead>
              <tbody>
                  @foreach ($students as $student )
                      
                  
                <tr class="fw-normal">
                  <th>
                    <img src="{{asset('public\students/'.$student['photo'])}}" alt="avatar "
                      style="width: 45px; height: auto;">
                    <span class="ms-2">{{$student['fname']}} {{$student['lname']}}</span>
                  </th>
                  <td class="align-middle">
                    <span >{{$student['birth_day']}}</span >
                  </td>
                  <td class="align-middle">
                    <span >{{$student['gender']}}</span >
                  </td>
                  <td class="align-middle">
                    <span >{{$student['email']}}</span >
                  </td>
                  <td class="align-middle">
                    <span >{{$student['phone']}}</span >
                  </td>
                  <td class="align-middle">
                    <span >{{$student['standard']}}</span >
                  </td>
                  <td class="align-middle">
                    <span >{{$student['joining_date']}}</span >
                  </td>
                  <td class="align-middle">
                    <a href="#!" data-mdb-toggle="tooltip" title="Done"><i class="fas fa-check fa-lg text-success me-3"></i></a>
                    <a href="#!" data-mdb-toggle="tooltip" title="Remove"><i class="fas fa-trash-alt fa-lg text-warning"></i></a>
                  </td>
                </tr>
               
                @endforeach
              </tbody>
            </table>


          </div>
        </div>

      </div>
    </div>
  </div>
</section>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

   
  </body>
</html>
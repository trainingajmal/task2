<?php

namespace App\Http\Controllers;

use App\Models\Member;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    function addData(Request $req){
        $req->validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'birthday'=>'required',
            'gender'=>'required',
            'email' => 'required|email:rfc,dns',
            'phone' => 'required | numeric | digits:10 | starts_with: 6,7,8,9',
            'standard'=>'required',
            'joindate'=>'required'
        ]);
        $member=new Member;
        $member->fname=$req->input('first_name');
        $member->lname=$req->input('last_name');
        $member->birth_day=$req->input('birthday');
        
        $member->gender=$req->input('gender');
        $member->email=$req->input('email');
        $member->phone=$req->input('phone');
        $member->standard=$req->input('standard');
        $member->joining_date=$req->input('joindate');
        if($req->hasFile('file')){
            $file=$req->file('file');
            $extension=$file->getClientOriginalExtension();
            $filename=time().'.'.$extension;
            $file->move('public/students/',$filename);
            $member->photo=$filename;
        }     
        
       
        $member->save();
        return redirect('list');

    
    }
    function show(){
        $data=Member::all();
        return view('list',['students'=>$data]);
    }

}
